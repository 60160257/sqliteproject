/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author acer
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (1, 'user1', 1231);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (2, 'user2', 1232);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (3, 'user3', 1233);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (4, 'user4', 1234);";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();

        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }

    }
}
